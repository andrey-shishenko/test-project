#!/bin/bash
# scritp for installation on Ubuntu (tested on Ubuntu 14.04)
# script installs virtualbox, vagrant, ansible

#apt-get install virtualbox -y
echo "deb http://download.virtualbox.org/virtualbox/debian trusty contrib" >> /etc/apt/sources.list
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
apt-get update -y
apt-get install virtualbox-5.0 -y
apt-get install dkms -y

#apt-get install vagrant -y
wget https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1_x86_64.deb
dpkg -i vagrant_1.8.1_x86_64.deb

#apt-get install ansible -y
apt-get install software-properties-common -y
apt-add-repository ppa:ansible/ansible -y
apt-get update
apt-get install ansible -y